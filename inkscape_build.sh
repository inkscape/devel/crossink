#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Build Inkscape by cross-compiling it from Linux to Windows.

### shellcheck #################################################################

# Nothing here.

### dependencies #################################################################

SELF_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1; pwd)
for CONFIG_ITEM in $("$SELF_DIR"/run-parts list "$SELF_DIR"/conf/'*.sh'); do
  # shellcheck disable=SC1090 # can't point to a single source here
  source "$CONFIG_ITEM"
done
unset CONFIG_ITEM

source "$SELF_DIR"/src/crossroad.sh
source "$SELF_DIR"/src/ink.sh

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

mkdir -p "$CCACHE_DIR"

if [ -d "$INK_SRC_DIR" ]; then
  echo "using existing INK_DIR=$INK_SRC_DIR"
else
  ink_install_source
fi

if [ -d "$INK_BLD_DIR" ]; then
  rm -rf "$INK_BLD_DIR"
fi
mkdir -p "$INK_BLD_DIR"

rsync -a --mkpath "$CROSSROAD_PREFIX"/etc/fonts ./etc/

ink_patches_apply

COUNT=2
cat <<EOF | crossroad w64 inkscape --run="-"
  cd "$INK_BLD_DIR" || exit 1

  COUNT=$COUNT
  while \
      ! crossroad cmake \
        -DCMAKE_C_COMPILER_LAUNCHER=ccache \
        -DCMAKE_CXX_COMPILER_LAUNCHER=ccache \
        -DWITH_INTERNAL_CAIRO=OFF \
        -DHAVE_MINGW64=ON \
        -DMINGW_ARCH=x86_64-w64-mingw32 \
        -DMINGW_PATH="$SHR_DIR"/crossroad/roads/w64/inkscape \
        -DMINGW_BIN="$SHR_DIR"/crossroad/roads/w64/inkscape/bin \
        -DMINGW_LIB="$SHR_DIR"/crossroad/roads/w64/inkscape/lib \
        -DCMAKE_INSTALL_PREFIX="$WRK_DIR"/inkscape \
        -GNinja \
        "$INK_SRC_DIR" \
      && [ \$COUNT -gt 0 ] \
      ; do
    # cmake fails the first time, so try again
    ((COUNT--))
    echo "cmake failed, remaining attempts: \$COUNT"
  done

  ninja
  ninja install

  wine64 "\$CROSSROAD_PREFIX"/bin/gdk-pixbuf-query-loaders.exe > \
    "\$CROSSROAD_PREFIX"/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache
  cp "\$CROSSROAD_PREFIX"/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache \
    "$WRK_DIR"/inkscape/lib/gdk-pixbuf-2.0/2.10.0
EOF

ink_patches_undo
