# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install and use crossroad. https://pypi.org/project/crossroad/

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

CROSSROAD_URL=git://git.tuxfamily.org/gitroot/crossroad/crossroad.git
CROSSROAD_VER=v0.9.0-15-gf6031ef

CROSSROAD_SRC_DIR=$SRC_DIR/crossroad

### functions ##################################################################

function crossroad_install
{
  mkdir -p "$CROSSROAD_SRC_DIR"
  git clone $CROSSROAD_URL "$CROSSROAD_SRC_DIR"
  git -C "$CROSSROAD_SRC_DIR" checkout $CROSSROAD_VER
  "$CROSSROAD_SRC_DIR"/setup.py install --prefix="$VER_DIR"
}

function crossroad_install_inkscape_deps
{
  cat << EOF | "$BIN_DIR"/crossroad w64 inkscape --run="-"
    crossroad source msys2

    # mandatory dependencies
    crossroad install \
      boost \
      double-conversion \
      gc \
      gdb \
      gsl \
      gtkmm3 \
      lcms2 \
      libsoup \
      libxslt

    # optional dependencies
    crossroad install \
      aspell \
      aspell-en \
      gspell \
      libcdr \
      libwpg \
      libvisio \
      poppler \
      potrace

    # Python dependencies for Inkscape
    crossroad install \
      python-coverage \
      python-cssselect \
      python-gobject \
      python-lxml \
      python-numpy \
      python-pillow \
      python-pip \
      python-pyserial \
      python-packaging \
      python-six \
      scour

    # Python dependencies for Extension Manager
    crossroad install \
      python-appdirs \
      python-msgpack \
      python-lockfile \
      python-cachecontrol \
      python-idna \
      python-urllib3 \
      python-chardet \
      python-certifi \
      python-requests
EOF
}

### main #######################################################################

# Nothing here.
