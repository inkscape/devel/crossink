# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Set up Inkscape sources.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # multipe vars only used outside this script

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

INK_URL=${INK_URL:-https://gitlab.com/inkscape/inkscape}
INK_BRANCH=${INK_BRANCH:-master}

INK_SRC_DIR=${INK_SRC_DIR:-$SRC_DIR/inkscape}
INK_BLD_DIR=${INK_BLD_DIR:-$VAR_DIR/build/inkscape}

### functions ##################################################################

function ink_install_source
{
  echo "installing source to $INK_SRC_DIR"

  git clone \
    --branch "$INK_BRANCH" \
    --single-branch \
    --depth 1 \
    --recurse-submodules \
    "$INK_URL" "$INK_SRC_DIR"
}

function ink_patches_apply
{
  # For the time being we need to patch some scripts.
  (
    cd "$INK_SRC_DIR" || exit 1

    if !  patch --dry-run --batch -p1 < "$SELF_DIR"/src/crossroad.patch |
          grep "previously applied"; then
      patch -p1 < "$SELF_DIR"/src/crossroad.patch
    fi
  )
}

function ink_patches_undo
{
  # For the time being we need to patch some scripts.
  (
    cd "$INK_SRC_DIR" || exit 1

    if  patch --dry-run --batch -p1 < "$SELF_DIR"/src/crossroad.patch |
        grep "previously applied"; then
      patch -R -p1 < "$SELF_DIR"/src/crossroad.patch
    fi
  )
}

### main #######################################################################

# Nothing here.
