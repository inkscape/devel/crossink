#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Install mingw dependencies with crossroad.

### shellcheck #################################################################

# Nothing here.

### dependencies #################################################################

SELF_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1; pwd)
for CONFIG_ITEM in $("$SELF_DIR"/run-parts list "$SELF_DIR"/conf/'*.sh'); do
  # shellcheck disable=SC1090 # can't point to a single source here
  source "$CONFIG_ITEM"
done
unset CONFIG_ITEM

source "$SELF_DIR"/src/crossroad.sh

unset SELF_DIR

### variables ##################################################################

RELEASE_TAG=$1

CROSSINK_RELEASE=https://gitlab.com/api/v4/projects/39095610/packages/generic/\
crossink/$RELEASE_TAG/crossink.tar.xz

### functions ##################################################################

# Nothing here.

### main #######################################################################

if [ -z "$RELEASE_TAG" ]; then
  # Install everything from scratch, i.e. pull everything from current msys2.
  # Since msys2 is rolling-release, you might end up using different versions
  # of libraries each time you run this.

  xdg_create_dirs

  crossroad_install
  crossroad_install_inkscape_deps
else
  # Use a release artifact. This will always give you a fixed set of verions
  # of libraries. (the tag is YYMMDD of when the libraries were pulled)

  mkdir -p "$WRK_DIR"
  if curl -L "$CROSSINK_RELEASE" | tar -C "$WRK_DIR" -xJp; then
    if ! $CI_GITLAB; then
      # The release artifact is tailored towards GitLab, so if we're not running
      # in GitLab CI, we need to...

      # ...re-run installation to fix symlinks
      "$CROSSROAD_SRC_DIR"/setup.py install --prefix="$VER_DIR"
      # ...adjust hard-coded path from the build environment to current VER_DIR
      find "$SHR_DIR"/crossroad -type f -print0 \
        \( -name "*.pc" -o -name "*.py" -o -name "*-config" \) |
        xargs -0 sed -i "s|/builds/inkscape/devel/crossink/crossink|$VER_DIR|g"
    fi
  else
    echo "invalid release tag: $RELEASE_TAG"
    exit 1
  fi
fi
