# cross-compile Inkscape

![pipeline status](https://gitlab.com/inkscape/devel/crossink/badges/main/pipeline.svg)
![Latest Release](https://gitlab.com/inkscape/devel/crossink/-/badges/release.svg)

Build the Windows version of Inkscape on Linux using [crossroad](https://pypi.org/project/crossroad/) and [msys2](https://www.msys2.org).

This is being developed on Arch Linux as main development platform and Ubuntu as secondary platform on best-effort basis. CI jobs are provided to demonstrate the viability of this work and also create "point in time"-releases of the dependencies we use from [msys2](https://www.msys2.org) for better reproducability.

## usage

1. Install the dependencies required to run [crossroad](https://pypi.org/project/crossroad/) for mingw-w64 on your Linux distribution. Check the [CI configuration](.gitlab-ci.yml) (`.mingw_arch`, `.mingw_ubuntu`) to see the list of packages for Arch and Ubuntu as examples.

    💁 _This is the only step that requires you to perform changes to your OS. Everything else is self-contained and does not touch anything outside its designated work directory._

1. Decide where you want to place the work directory. This is where all the building and additional installations are going to happen. About 3.3 GiB of disk space will be required, subject to change (read: increase) with ongoing development.

    ```bash
    # This is optional; the default is '/tmp/work'.
    export WRK_DIR=/home/myuser/mybuilddirectory
    ```

    💁 _You cannot change or move the work directory afterwards. You will need to re-run this step and all the following ones if you want to pick a new location._


1. Install Inkscape's build dependencies.

    ```bash
    # This will pull in latest dependencies from msys2. Alternatively you can
    # specify a tag to use a fixed set of dependencies, e.g.
    #   ./install_deps.sh m220904
    # See releases section on GitLab.
    ./install_deps.sh
    ```

1. Set Inkscape source directory.

    ```bash
    # This is also optional, but you very likely want to point this to your own
    # Inkscape sources. If you don't set this, current master branch will be 
    # shallow-cloned and built.
    export INK_SRC_DIR=/path/to/my/repo
    ```

1. Build Inkscape.

    ```bash
    ./inkscape_build.sh
    ```

    💁 _As a temporary workaround, this will also apply patches to Inkscape's sources and undo them afterwards._

1. Create 7zip-packaged distributable artifact.

    ```bash
    ./inkscape_distrib.sh
    ```

    💁 _As a temporary workaround, this will also apply patches to Inkscape's sources and undo them afterwards._

## license

[GPL-2.0-or-later](LICENSE)
