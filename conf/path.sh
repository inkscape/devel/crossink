# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Set PATH.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced

### variables ##################################################################

export PATH=$BIN_DIR:$PATH

### functions ##################################################################

# Nothing here.

### main #######################################################################

# Nothing here.
