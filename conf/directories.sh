# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Directory layout.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # we only use exports if we really need them

### variables ##################################################################

WRK_DIR=${WRK_DIR:-/tmp/work}
VER_DIR=${VER_DIR:-$WRK_DIR/crossink}

BIN_DIR=${BIN_DIR:-$VER_DIR/bin}
ETC_DIR=${ETC_DIR:-$VER_DIR/etc}
SHR_DIR=${SHR_DIR:-$VER_DIR/share}
VAR_DIR=${VAR_DIR:-$VER_DIR/var}

USR_DIR=${USR_DIR:-$VER_DIR/usr}
SRC_DIR=${SRC_DIR:-$USR_DIR/src}

if [ -z "$ARTIFACT_DIR" ]; then
  if   $CI_GITHUB; then
    ARTIFACT_DIR=$GITHUB_WORKSPACE
  elif $CI_GITLAB; then
    ARTIFACT_DIR=$CI_PROJECT_DIR
  else
    ARTIFACT_DIR=$WRK_DIR
  fi
fi

### functions ##################################################################

# Nothing here.

### main #######################################################################

# Nothing here.
