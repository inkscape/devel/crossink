#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Package Inkscape into a releaseable artifact using 7zip.

### shellcheck #################################################################

# Nothing here.

### dependencies #################################################################

SELF_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" || exit 1; pwd)
for CONFIG_ITEM in $("$SELF_DIR"/run-parts list "$SELF_DIR"/conf/'*.sh'); do
  # shellcheck disable=SC1090 # can't point to a single source here
  source "$CONFIG_ITEM"
done
unset CONFIG_ITEM

source "$SELF_DIR"/src/crossroad.sh
source "$SELF_DIR"/src/ink.sh

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

ink_patches_apply

cat <<EOF | crossroad w64 inkscape --run="-"
  cd "$INK_BLD_DIR" || exit 1
  ninja dist-win-7z-fast
EOF

ink_patches_undo

mv "$INK_BLD_DIR"/inkscape-*.7z "$ARTIFACT_DIR"
